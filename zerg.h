/* Filename: zerg.h
 * Blast parser
 * Apua Cesar de Miranda Paquola <apua@iq.usp.br>
 */ 

#ifndef _zerg_h_
#define _zerg_h_

#define BLAST_VERSION 1   /* Esta tem que ser a primeira */
#define QUERY_NAME 2
#define QUERY_ANNOTATION 3
#define QUERY_LENGTH 4
#define NOHITS 5
#define DESCRIPTION_HITNAME 6
#define DESCRIPTION_ANNOTATION 7
#define DESCRIPTION_SCORE 8
#define DESCRIPTION_EVALUE 9
#define SUBJECT_NAME 10
#define SUBJECT_ANNOTATION 11
#define SUBJECT_LENGTH 12
#define SCORE_BITS 13
#define SCORE 14
#define EVALUE 15
#define IDENTITIES 16
#define ALIGNMENT_LENGTH 17
#define PERCENT_IDENTITIES 18
#define GAPS 19
#define QUERY_ORIENTATION 20
#define SUBJECT_ORIENTATION 21
#define QUERY_START 22
#define QUERY_END 23
#define SUBJECT_START 24
#define SUBJECT_END 25
#define END_OF_REPORT 26
#define POSITIVES 27
#define PERCENT_POSITIVES 28
#define QUERY_FRAME 29
#define SUBJECT_FRAME 30
#define UNMATCHED 31    /* Esta tem que ser a ultima */

#ifdef __cplusplus
extern "C" {
#endif

void zerg_open_file(char* filename);
void zerg_close_file();
void zerg_ignore(int code);
void zerg_ignore_all();
void zerg_unignore(int code);
void zerg_unignore_all();
int zerg_get_token(int* code, char **value);
int zerg_get_token_offset();

#ifdef __cplusplus
}
#endif

#endif
